﻿abstract class SomeObject {
}

class ConcreteSomeObject extends SomeObject {
}

abstract class PatternB {
    public abstract Create(): SomeObject;
}

class ConcretePatternB extends PatternB {
    public Create(): SomeObject {
        return new ConcreteSomeObject();
    }
}

/* Write your answers and comments below this line

*/
