﻿class PatternA {
    private static _instance: PatternA | null;

    private constructor() {
    }

    public static GetInstance(): PatternA {
        if (this._instance == null)
            this._instance = new PatternA();

        return this._instance;
    }

    public DoWork(): void {
        throw new Error("Not implemented");
    }
}

/* Write your answers and comments below this line

*/
