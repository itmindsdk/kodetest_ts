﻿abstract class Foo {
    private readonly _list: Bar[] = [];

    public Attach(bar: Bar): void {
        this._list.push(bar);
    }

    public Detach(bar: Bar): void {
        const indexOf = this._list.indexOf(bar);
        if (indexOf !== 0) {
            this._list.splice(indexOf, 1);
        }
    }

    public Notify(): void {
        for (const o of this._list) {
            o.Update();
        }
    }
}

abstract class Bar {
    protected readonly Foo: Foo;

    protected constructor(foo: Foo) {
        this.Foo = foo;
    }

    public abstract Update(): void;
}

/* Write your answers and comments below this line

*/
