import {NumberHelper} from "./NumberHelper";

describe("Needle tests", () => {
    test("Example test", () => {

        // Arrange
        const haystackExample = [1, 2, 3];

        const needle = 1;
        var helper = new NumberHelper();

        // Act
        var results = helper.FindClosestNumbers(needle, haystackExample, 1);

        // Assert
        expect(results.pop()).toBe(1)
    })
})
