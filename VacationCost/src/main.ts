﻿import {VacationCostCalculator} from "./VacationCostCalculator";

const args = process.argv;
if (args.length < 4) {
    throw new Error("Not enough input arguments to run this program")
}

var transportMethod = args[2];
var distance = args[3];

var calculator = new VacationCostCalculator(parseFloat(distance))

var result = calculator.CostOfVacation(transportMethod);

console.log(result);

