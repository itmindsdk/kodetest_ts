﻿export class VacationCostCalculator {
    constructor(public distanceToDestination: number) {}

    public CostOfVacation(transportMethod: string): number {
        switch (transportMethod) {
            case "Car":
                return (this.distanceToDestination * 1);
            case "Plane":
                return (this.distanceToDestination * 2);
            default:
                throw new Error("Argument out of range");
        }
    }
}
