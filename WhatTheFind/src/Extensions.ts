﻿/**
* Find any node in an object graph that satisfy a given predicate and return it.
* @param root The root node
* @param predicate The given condition to satisfy
* @param getChildren Child selector
* @returns Node satisfying the condition, else null.
*/
export function FindWhere<T extends object>(root: T, predicate: (el: T) => boolean, getChildren: (el: T) => T[] | null): T | null {
    // YOUR SOLUTION GOES HERE
    throw new Error("Not implemented");
}
