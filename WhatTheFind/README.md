# What the Find

You are looking through an object graph for an object that satisfies a given predicate.

![Object graph](images/object_graph.png)

You don't know these objects. So a function (`getChildren`) that will return the next potential object(s) in the graph chain is provided.

You're supposed to implement the following method.

```ts
public FindWhere<T extends object>(root: T, predicate: (el: T) => boolean, getChildren: (el: T) => T[] | null): T | null {
    // YOUR SOLUTION GOES HERE
    throw new Error("Not implemented");
}
```

One possible usage of the function could be as follows, but not limited to:

```ts
import {FindWhere} from "./WhatTheFind/src/Extensions";

class NodeImpl {
    constructor(public Value: number, public Children: NodeImpl[] | null = null){}
}

var nodeB = new NodeImpl(1);
var nodeA = new NodeImpl(0, [nodeB]);

FindWhere(nodeA, x => x.Value == 1, x => x.Children); // returns nodeB
```

# Solution

Implement your solution in the `Extensions` file.
If you see `// YOUR SOLUTION GOES HERE` you've found the right place :-)

## What you'll be evaluated on

You'll be evaluated on your ability to solve this problem but also on how you solve it.
