﻿import {Customers} from "./Customers";

export interface IFinder {
    FromRight(customers: Customers, numberFromRight: number): string
}
