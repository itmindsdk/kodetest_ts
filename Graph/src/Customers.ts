﻿export class Customers {
    public Next: Customers | null;
    public Person: string;

    private constructor(next: Customers | null, person: string) {
        this.Next = next;
        this.Person = person;
    }


    public Previous(person: string): Customers {
        return new Customers(this, person);
    }

    public static Create(person: string): Customers {
        return new Customers(null, person);
    }
}
