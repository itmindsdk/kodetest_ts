﻿import {Customers} from "./Customers";

let currentCustomer: Customers | null = Customers
    .Create("Kim")
    .Previous("Hans")
    .Previous("Ole")
    .Previous("Peter");

while (currentCustomer != null) {
    if (currentCustomer.Next != null)
        console.log(currentCustomer.Person + " -> ");
    else
        console.log(currentCustomer.Person);

    currentCustomer = currentCustomer.Next;
}
